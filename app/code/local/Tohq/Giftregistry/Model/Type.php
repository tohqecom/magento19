<?php
class Tohq_Giftregistry_Model_Type extends Mage_Core_Model_Abstract
{
   public function __construct()
   {
     $this->_init('tohq_giftregistry/type');
     parent::_construct();
   }
}