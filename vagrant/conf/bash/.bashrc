# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions
alias phpunit="php vendor/phpunit/phpunit/phpunit"
alias codecept="php vendor/codeception/codeception/codecept"

export GIT_SSL_NO_VERIFY="1"
